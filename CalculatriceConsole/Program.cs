﻿using System;

namespace CalculatriceConsole
{
    public class Program
    {
        public static int Multiplication(int facteur_gauche, int facteur_droite)
        {
            facteur_gauche = Math.Abs(facteur_gauche);
            facteur_droite = Math.Abs(facteur_droite);
            return facteur_gauche*facteur_droite;
        }

          public static int Somme(int facteur_gauche, int facteur_droite)
        {
            facteur_gauche = Math.Abs(facteur_gauche);
            facteur_droite = Math.Abs(facteur_droite);
            return facteur_gauche+  facteur_droite; 
        }
        public static int Soustraction(int facteur_gauche, int facteur_droite)
        {
            facteur_gauche = Math.Abs(facteur_gauche);
            facteur_droite = Math.Abs(facteur_droite);
            return facteur_gauche - facteur_droite;
        }
        public static float Division(int facteur_gauche, int facteur_droite)
        {
            facteur_gauche = Math.Abs(facteur_gauche);
            facteur_droite = Math.Abs(facteur_droite);
            if (facteur_gauche == 0 || facteur_droite == 0)
            {
                return 0;
            }
            return (float)facteur_gauche / (float)facteur_droite;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}