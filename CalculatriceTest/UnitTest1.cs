using System;
using Xunit;

using CalculatriceConsole;

namespace calculatriceTest
{
    public class UnitTest1
    {
        [Fact]
        public void TestMultiplication()
        {
            Assert.Equal(4, Program.Multiplication(2,2));
            Assert.Equal(21, Program.Multiplication(7,3));
            Assert.Equal(8, Program.Multiplication(-4,2));
        }
        [Fact]
        public void TestAddition()
        {
            Assert.Equal(5, Program.Somme(2,3));
            Assert.Equal(10, Program.Somme(7,3));
            Assert.Equal(6, Program.Somme(-4,2));
        }
        [Fact]
        public void TestSoustraction()
        {
            Assert.Equal(-1, Program.Soustraction(2,3));
            Assert.Equal(4, Program.Soustraction(7,3));
            Assert.Equal(2, Program.Soustraction(-4,2));
        }
         [Fact]
        public void TestDivision()
        {
            Assert.Equal(2, Program.Division(4,2));
            Assert.Equal(3.5, Program.Division(7,2));
            Assert.Equal(3, Program.Division(-6,2));
            Assert.Equal(0, Program.Division(0,2));
            Assert.Equal(0, Program.Division(2,0));
        }
    }
}
